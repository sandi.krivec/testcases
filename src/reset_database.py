import shutil
import subprocess
# from typing import Tuple
import logging
from pathlib import Path

CWD = Path(__file__).resolve().parent
TESTCASES_MIGRATION_DIR = CWD / "testcases" / "migrations"

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(module)s:%(lineno)s %(levelname)-15s %(message)s", datefmt="%H:%M:%S", encoding="utf8")
_log = logging.getLogger("reset_database")


def execute_command(command: str, cwd: Path = CWD) -> str:
    """TODO

    Args:
        command: TODO
        cwd: TODO. Defaults to CWD.

    Raises:
        Exception: TODO

    Returns:
        standard output
    """
    res = subprocess.run(command, cwd=cwd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    if res.returncode:
        raise Exception(res.stderr)
    _log.info(res.stdout.strip())
    return res.stdout


if __name__ == "__main__":
    _log.info("Flushing database")
    execute_command("python manage.py flush --noinput")  # flush wil
    # remove existing migrations
    if TESTCASES_MIGRATION_DIR.exists():
        _log.info(f"removing {TESTCASES_MIGRATION_DIR}")
        shutil.rmtree(TESTCASES_MIGRATION_DIR)  # this will remove migrations folder, exception is raised if folder does not exists
    else:
        _log.info(f"{TESTCASES_MIGRATION_DIR} does not exist")
    execute_command("python manage.py makemigrations testcases")
    execute_command("python manage.py migrate")
    execute_command("python manage.py shell -c \"from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')\"")
    execute_command("python manage.py loaddata Testcase.json")
    execute_command("python manage.py loaddata TestcaseResult.json")
