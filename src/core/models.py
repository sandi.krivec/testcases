from django.db import models
from django.utils import timezone


class TimeStampedModel(models.Model):
    creation_date = models.DateTimeField(editable=False, default=timezone.now)
    last_modified = models.DateTimeField(editable=False, default=timezone.now)

    def save(self, *args, **kwargs):
        if not self.creation_date:
            self.creation_date = timezone.now()

        self.last_modified = timezone.now()
        return super(TimeStampedModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True
