from django.contrib import admin

# Register your models here.

from .models import Testcase, TestcaseResult


class TestcaseAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Testcase._meta.fields]


class TestcaseResultAdmin(admin.ModelAdmin):
    list_display = [f.name for f in TestcaseResult._meta.fields]


admin.site.register(Testcase, TestcaseAdmin)
admin.site.register(TestcaseResult, TestcaseResultAdmin)
