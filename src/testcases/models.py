# import uuid as uuid_lib
from django.db import models
from django.conf import settings
from core.models import TimeStampedModel


class Project(models.TextChoices):
    NOT_STARTED = 'not started', 'Not started'  # 2. Displayed on Django Admin
    OBSOLETE = 'obsolete', 'Obsolete'
    REQUIRES_ATTENTION = 'requires attention', 'Requires attention'
    WORK_IN_PROGRESS = 'work in progress', 'Work in progress'
    READY_FOR_EXECUTION = 'ready for execution', 'Ready for execution'
    COMPLETED = 'completed', 'Completed'


class Testcase(models.Model):
    # uuid = models.UUIDField(default=uuid_lib.uuid4, editable=False)
    # slug = models.SlugField(max_length=255, unique=True)
    tc_id = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    # pub_date = models.DateTimeField("date published")

    def __str__(self):
        return f"TC_{self.tc_id}_{self.name}"


class TestcaseResult(TimeStampedModel):
    class Statuses(models.TextChoices):
        NOT_STARTED = 'not started', 'Not started'  # 2. Displayed on Django Admin
        OBSOLETE = 'obsolete', 'Obsolete'
        REQUIRES_ATTENTION = 'requires attention', 'Requires attention'
        WORK_IN_PROGRESS = 'work in progress', 'Work in progress'
        READY_FOR_EXECUTION = 'ready for execution', 'Ready for execution'
        COMPLETED = 'completed', 'Completed'
    status = models.CharField(choices=Statuses.choices, default=Statuses.NOT_STARTED, max_length=50)

    # uuid = models.UUIDField(default=uuid_lib.uuid4, editable=False)
    assignee = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True)
    testcase = models.ForeignKey(Testcase, on_delete=models.PROTECT)
    location = models.CharField(max_length=400)
    # pub_date = models.DateTimeField("date published")

    def __str__(self):
        return self.location
