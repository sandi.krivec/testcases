from django.http import HttpResponse
from django.template import loader
from django.views.generic.list import ListView

from .models import TestcaseResult


class TestcaseResultList(ListView):
    template_name = 'testresult.html'  # html file to display the list of results
    model = TestcaseResult
    context_object_name = 'testcase_result'  # used in the HTML template to loop through and list results


def index(request):
    query_results = TestcaseResult.objects.all()
    template = loader.get_template("index.html")
    context = {
        "test_results": query_results,
    }
    return HttpResponse(template.render(context, request))
