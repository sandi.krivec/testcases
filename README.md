# How to start

- create venv `python -m venv venv`
- activate venv `venv\Scripts\activate.bat`
- install libs `pip install -r requirements.txt`
- run `src\reset_database.py` # it will prepare database and `admin` user with `admin` password
- start Django server with launch config or type `python src/manage.py runserver`
- goto http://127.0.0.1:8000/
- admin http://127.0.0.1:8000/admin

# Django commands

- start project `python -m django startproject config ` # rename main folder to src
- create venv `python -m venv venv`
- activate venv `venv\Scripts\activate.bat`
- install libs `pip install -r requirements.txt`
- cd src
- create database `python manage.py migrate`
- create superuser `python manage.py createsuperuser` # http://127.0.0.1:8000/admin/
- run server `python manage.py runserver`

## additonal

- create new model `python manage.py startapp testcases`
- add app to INSTALLED_APPS in settings.py.
- after change to model `python manage.py makemigrations testcases`
- to apply those changes to the database. `python manage.py migrate`

## data

- import data to database `python manage.py loaddata Testcase.json`
- flush db `python manage.py flush`
- export db `python manage.py dumpdata --exclude auth.permission --exclude contenttypes > db.json`
